import { TestBed, inject, async, ComponentFixture, tick, fakeAsync } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA, DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { LogService } from '../core/services/log.service';
import { SimpleService } from '../core/services/simple.service';
import { HomeComponent } from './home.component';
import { SimpleModel } from '../core/models/simple.model';

let component: HomeComponent;
let fixture: ComponentFixture<HomeComponent>;
let welcomeMessageElement: DebugElement;
let errorMessageElement: DebugElement;

describe('HomeComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientModule ],
      declarations: [ HomeComponent ],
      providers: [ SimpleService, LogService ],
      schemas: [ NO_ERRORS_SCHEMA ],
    }).compileComponents();
  }));

  beforeEach(() => {
    // create component and test fixture
    fixture = TestBed.createComponent(HomeComponent);

    // get test component from the fixture
    component = fixture.componentInstance;

    welcomeMessageElement = fixture.debugElement.query(By.css('#welcomeMessage'));
    errorMessageElement = fixture.debugElement.query(By.css('#errorMessage'));
  });

  it('getWelcomeMessage - should receive a message and shown to the user',
  fakeAsync(inject([SimpleService], (simpleService: SimpleService) => {
    const fakeResult = new SimpleModel();
    fakeResult.message = 'fakeHi';

    spyOn(simpleService, 'getHi').and.returnValue(Promise.resolve(fakeResult));

    component.ngOnInit();

    // wait for promises
    tick();

    // detect changes
    fixture.detectChanges();
    expect(welcomeMessageElement.nativeElement.textContent).toBe(fakeResult.message);
    expect(errorMessageElement.nativeElement.textContent).toBe('');
  })));

  it('getWelcomeMessage - should receive an error and shown to the user',
    fakeAsync(inject([SimpleService], (simpleService: SimpleService) => {
    const fakeResult = new SimpleModel();
    fakeResult.error = 'error!';

    spyOn(simpleService, 'getHi').and.returnValue(Promise.reject(fakeResult));

    component.ngOnInit();

    // wait for promises
    tick();

    // detect changes
    fixture.detectChanges();
    expect(welcomeMessageElement.nativeElement.textContent).toBe('');
    expect(errorMessageElement.nativeElement.textContent).toBe(fakeResult.error);
  })));
});
