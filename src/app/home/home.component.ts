import { Component, OnInit } from '@angular/core';
import { SimpleService } from '../core/services/simple.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.component.html',
})
export class HomeComponent implements OnInit {
  message: string;
  errorMessage: string;

  constructor(private simpleService: SimpleService) {  }

  ngOnInit(): void { this.getWelcomeMessage(); }

  getWelcomeMessage() {
    this.simpleService.getHi().then(res => {
      this.message = res.message;
    }, error => {
      // Handle error here
      this.errorMessage = error.error;
    });
  }
}
