import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule, provideRoutes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';

import { SimpleService } from './core/services/simple.service';
import { LogService } from './core/services/log.service';
import { AppRouting } from './app-routing';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    HttpClientModule,
    AppRouting
  ],
  providers: [ SimpleService, LogService ],
  bootstrap: [ AppComponent ],
  exports: [RouterModule]
})
export class AppModule { }
