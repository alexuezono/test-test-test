import { LogService } from './log.service';

let logService: LogService;

beforeEach(() => {
  logService = new LogService();
});

describe('LogService', () => {

  it('logSomething - should log: log log log', () => {
    expect(logService.logSomething('log log log')).toMatch('Log Service: log log log');
  });

});
