import { Injectable } from '@angular/core';
import { LogService } from './log.service';
import { HttpClient } from '@angular/common/http';
import { SimpleModel } from '../models/simple.model';

@Injectable()
export class SimpleService {
  constructor(private logService: LogService, private http: HttpClient) {  }

  getHi(): Promise<SimpleModel> {
    this.logService.logSomething('calling getHi');

    const apiURL = `http:\\takaservice\gethi`;
    return new Promise((resolve, reject) => {
      const result = new SimpleModel();

      this.http.get(apiURL)
          .toPromise()
          .then(
              res => { // Success
                result.error = '';
                result.message = <any>res;
                resolve(result);
              }
          ).catch(function (data) {
            // Handle error here
            result.error = <any>data.message;
            result.message = '';
            reject(result);
        });
    });
  }
}
