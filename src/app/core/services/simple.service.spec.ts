import { TestBed, getTestBed } from '@angular/core/testing';
import { LogService } from './log.service';
import { SimpleService } from './simple.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

let logService: LogService;
let simpleService: SimpleService;

let injector: TestBed;
let httpMock: HttpTestingController;

describe('SimpleService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [SimpleService, LogService]
    });
    injector = getTestBed();
    simpleService = injector.get(SimpleService);
    logService = injector.get(LogService);
    httpMock = injector.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('getHi - should receive a message from service', () => {
    spyOn(logService, 'logSomething').and.returnValue('fake log return');

    simpleService.getHi().then(res => {
      expect(res.error).toEqual('');
      expect(res.message).toEqual('Hi There! :D');
    });

    // mocking http response
    const req = httpMock.expectOne(`http:\\takaservice\gethi`);
    req.flush('Hi There! :D');
  });

  it('getHi - should receive an error from service', () => {
    spyOn(logService, 'logSomething').and.returnValue('fake log return');

    simpleService.getHi().then(res => { },
    msg => { // Error
      expect(msg.error).toContain('Unauthorized');
      expect(msg.message).toEqual('');
    });

    // mocking http response
    const req = httpMock.expectOne(`http:\\takaservice\gethi`);
    req.flush(null, { status: 401, statusText: 'Unauthorized' });
  });
});
