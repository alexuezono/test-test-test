import { Injectable } from '@angular/core';

@Injectable()
export class LogService {
  constructor() {  }

  logSomething(message: string): string {
    return `Log Service: ${message}!`;
  }
}
